package common

import java.util.*

fun String.firstSignToLowerCase() = "${this.substring(0, 1).lowercase(Locale.getDefault())}${this.substring(1)}"