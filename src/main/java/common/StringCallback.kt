package common

fun interface StringCallback {
    fun stringCallback(string: String?)
}