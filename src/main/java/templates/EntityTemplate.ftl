package ${package}

import ${package}.${entityName}.Companion.${entityNameCaps}
import javax.jws.*;

@Entity(
    tableName = ${entityNameCaps},
    <#if parentTableName??>
    foreignKeys = [
        ForeignKey(
            entity = ${parentTableName}::class,
            parentColumns = arrayOf(${parentTableName}.${parentColumnConstant}),
            childColumns = arrayOf(${referenceColumnConstant}),
            onDelete = ForeignKey.${deleteStrategy},
            onUpdate = ForeignKey.${updateStrategy},
        ),
    ]
    <#else>
    </#if>
)
data class ${entityName}(
    <#list fields as field>
        <#if field?is_last>
        <#if field.fieldModel.primaryKey>
        @PrimaryKey
        </#if>
        @ColumnInfo(name = ${field.fieldNameCaps})
        var ${field.fieldModel.fieldName}: ${field.typeText}
        <#else>
        <#if field.fieldModel.primaryKey>
        @PrimaryKey
        </#if>
        @ColumnInfo(name = ${field.fieldNameCaps})
        var ${field.fieldModel.fieldName}: ${field.typeText},
        </#if>
    </#list>
) {
    companion object {
         const val ${entityNameCaps} = "${entityNameLower}"
        <#list fields as field>
         const val ${field.fieldNameCaps} = "${field.fieldNameLower}"
        </#list>
    }
}