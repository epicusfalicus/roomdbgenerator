package ${package}

@Database(
    entities = [
        <#list entities as entity>
                <#if entity?is_last>
                ${entity}::class
                <#else>
                ${entity}::class,
                </#if>
            </#list>
    ],
    version = DB_VERSION,
    exportSchema = false
)
abstract class ${databaseName} : RoomDatabase() {

    companion object {
        private var instance: AppDatabase? = null

        const val DB_NAME = "${databaseLowerName}"
        const val DB_VERSION = 1

        @Synchronized
        fun get(context: Context): ${databaseName} {
            if (instance == null) {
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    ${databaseName}::class.java,
                    DB_NAME
                )
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return instance!!
        }
    }

        <#list daoList as daoName, daoClass>
    abstract fun ${daoName}(): ${daoClass}
        </#list>
}