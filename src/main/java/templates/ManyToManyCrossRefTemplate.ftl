@Entity(primaryKeys = [${parentTableColumnConst}, ${entityTableColumnConst}])
data class ${crossRefTableName}(
    val ${parentTableColumnLowerName}: ${parentTableColumnType},
    val ${entityTableColumnLowerName}: ${entityTableColumnType}
)