data class ${parentTableName}With${entityTableName}(
    @Embedded val ${parentTableLowerName}: ${parentTableName},
    @Relation(
         parentColumn = ${parentTableColumnConst},
         entityColumn = ${entityTableColumnConst},
         associateBy = Junction(${crossRefTableName}::class)
    )
   val ${entityTableLowerName}s: List<${entityTableName}>
)

data class ${entityTableName}With${parentTableName}(
    @Embedded val ${entityTableLowerName}: ${entityTableName},
    @Relation(
         parentColumn = ${entityTableColumnConst},
         entityColumn = ${parentTableColumnConst},
         associateBy = Junction(${crossRefTableName}::class)
    )
   val ${parentTableLowerName}s: List<${parentTableName}>
)