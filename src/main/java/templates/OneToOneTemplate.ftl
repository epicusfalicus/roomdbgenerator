data class ${relClassName}(
    @Embedded val ${parentTableLowerName}: ${parentTableName},
    @Relation(
         parentColumn = ${parentTableColumnConst},
         entityColumn = ${childTableColumnConst}
    )
    val ${childTableLowerName}: ${childTableName}
)