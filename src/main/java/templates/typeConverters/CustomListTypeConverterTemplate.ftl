
@ProvidedTypeConverter
class ${className} {

    companion object {

        @TypeConverter
        @JvmStatic
        fun jsonToList(value: String?): List<${typeName}>? {
            if (value == null || value.isEmpty()) {
                return emptyList<${typeName}>()
            }

            val objects = Gson().fromJson(value, Array<${typeName}>::class.java) as Array<${typeName}>
            val list = objects.toList()

            return list
        }

        @TypeConverter
        @JvmStatic
        fun listToJson(value: List<${typeName}>?): String {
            var string = ""

            if (value == null || value.isEmpty()) {
                return string
            }

            return Gson().toJson(value)
        }
    }
}