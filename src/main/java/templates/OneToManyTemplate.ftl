data class ${relClassName}s(
    @Embedded val ${parentTableLowerName}: ${parentTableName},
    @Relation(
         parentColumn = ${parentTableColumnConst},
         entityColumn = ${childTableColumnConst}
    )
    val ${childTableLowerName}s: List<${childTableName}>
)