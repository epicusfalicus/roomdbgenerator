package ${package}

@Dao
interface ${daoName} {

    @Query("SELECT * FROM ${entityNameCaps}")
    fun getAll(): List<${entityName}>

    @Query("SELECT * FROM ${entityNameCaps} WHERE ${primaryKey} =: primaryKeyValue")
    fun getAllByPrimaryKey(primaryKeyValue: ${primaryKeyType}): List<${entityName}>

    @Query("DELETE * FROM ${entityNameCaps})"
    fun delete(entities: ${entityName})

    @Insert
    fun insertAll(entities: List<${entityName}>)

    @Query("SELECT * FROM ${entityNameCaps} WHERE ${primaryKey} =: primaryKeyValue")
    <#if asyncName == "Coroutines">
    private fun observeEntityByPrimaryKey(primaryKeyValue: ${primaryKeyType}): Flow<${entityName}>
    <#else>
    private fun observeEntityByPrimaryKey(primaryKeyValue: ${primaryKeyType}): Observable<${entityName}>
    </#if>

    private fun observeEntityByPrimaryKeyDistinctUntilChanged(primaryKeyValue: ${primaryKeyType}) =
        observeEntityByPrimaryKey(primaryKeyValue).distinctUntilChanged()
}