package storage

import model.EntityModel
import model.FieldModel
import model.RelationshipModel

object Storage {

    val createdEntityList: MutableList<EntityModel> = mutableListOf(
        EntityModel(
            "InspectionTable",
            fields = mutableListOf(FieldModel("table1Field1"), FieldModel("table1Field2"))
        ),
        EntityModel(
            "InspectorTable",
            fields = mutableListOf(FieldModel("table2Field1"), FieldModel("table2Field2"))
        ),
        EntityModel(
            "DocumentTable",
            fields = mutableListOf(FieldModel("table3Field1"), FieldModel("table3Field2"))
        ),
        EntityModel(
            "ReportTable",
            fields = mutableListOf(FieldModel("table3Field1"), FieldModel("table3Field2"))
        ),
        EntityModel(
            "FileTable",
            fields = mutableListOf(FieldModel("table3Field1"), FieldModel("table3Field2"))
        )
    )

    val relationshipList: MutableList<RelationshipModel> = mutableListOf()

    var databaseName: String = ""
    var generatePath: String = ""
}