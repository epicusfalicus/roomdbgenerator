package configurator

import analyser.DbParser
import analyser.EntityParser
import com.intellij.ide.ui.UINumericRange
import com.intellij.openapi.options.Configurable
import com.intellij.openapi.options.ConfigurationException
import forms.*
import generator.dao.DaoFileGenerator
import generator.dao.DaoFileGenerator.Companion.createDaoNameFromEntity
import generator.db.DbGenerator
import generator.entity.EntityGenerator
import generator.relationship.RelationshipGenerator
import generator.typeConverters.TypeConverterGenerator
import model.*
import org.jetbrains.annotations.Nls
import storage.Storage
import storage.Storage.createdEntityList
import storage.Storage.databaseName
import storage.Storage.generatePath
import storage.Storage.relationshipList
import javax.swing.JComponent
import javax.swing.JPanel
import javax.swing.JTextField
import javax.swing.JToggleButton

class Configurator : Configurable {

    val mainPanel = JPanel()

    @Nls(capitalization = Nls.Capitalization.Title)
    override fun getDisplayName(): String? {
        return null
    }

    override fun getHelpTopic(): String? {
        return null
    }

    override fun isModified(textField: JTextField, value: String): Boolean {
        return false
    }

    override fun isModified(textField: JTextField, value: Int, range: UINumericRange): Boolean {
        return false
    }

    override fun isModified(toggleButton: JToggleButton, value: Boolean): Boolean {
        return false
    }

    override fun getPreferredFocusedComponent(): JComponent? {
        return null
    }

    override fun createComponent(): JComponent {
        mainPanel.setBounds(0, 0, 1000, 1000)
        mainPanel.layout = null
        openInitializeVariantsForm()

        return mainPanel
    }

    override fun isModified(): Boolean {
        return false
    }

    @Throws(ConfigurationException::class)
    override fun apply() {
    }

    override fun reset() {}

    override fun cancel() {}

    private fun openEntityListForm() {
        mainPanel.removeAll()
        val entityListForm = EntityListForm(createdEntityList)
        val entityListPanel = entityListForm.entityListPanel
        entityListPanel.setBounds(10, 10, 580, 580)
        entityListForm.setOpenButtonClickListener {
            openEntityForm(entityListForm.currentSelectedEntityModel)
        }
        entityListForm.setCreateButtonClickListener {
            openEntityForm(EntityModel())
        }
        entityListForm.setGenerateButtonClickListener {
            startGenerating()
        }
        mainPanel.add(entityListPanel)
    }

    private fun openEntityForm(entityModel: EntityModel) {
        mainPanel.removeAll()
        val entityForm = EntityForm(entityModel)
        val rootJpanel = entityForm.rootJPanel
        rootJpanel.setBounds(10, 10, 580, 580)
        entityForm.columnsButton.addActionListener {}
        entityForm.setSaveEntityButtonClickListener {
            val newEntityModel = entityForm.entityModel
            newEntityModel.entityName = entityForm.entityName
            createdEntityList.removeAll { it.entityName == newEntityModel.entityName }
            createdEntityList.add(newEntityModel)
            openEntityListForm()
        }
        entityForm.setForeignKeyButtonClickListener {
            val newEntityModel = entityForm.entityModel
            openForeignKeyForm(newEntityModel)
        }

        entityForm.setRelationshipButtonClickListener {
            val newEntityModel = entityForm.entityModel
            openRelationshipKeyForm(newEntityModel)
        }

        mainPanel.add(rootJpanel)
    }

    private fun openForeignKeyForm(entityModel: EntityModel) {
        mainPanel.removeAll()
        val foreignKeyForm = ForeignKeyForm(entityModel)
        val foreignKeyJPanel = foreignKeyForm.foreignKeyJPanel
        foreignKeyJPanel.setBounds(10, 10, 580, 580)
        foreignKeyForm.setColumnsButtonListener {
            openEntityForm(foreignKeyForm.currentEntityModel)
        }

        mainPanel.add(foreignKeyJPanel)
    }

    private fun openRelationshipKeyForm(entityModel: EntityModel) {
        mainPanel.removeAll()
        val relForm = RelationshipForm(entityModel)
        val relJPanel = relForm.rootJPanel
        relJPanel.setBounds(10, 10, 580, 580)
        relForm.setColumnsButtonListener {
            openEntityForm(relForm.currentEntityModel)
        }

        mainPanel.add(relJPanel)
    }

    private fun startGenerating() {
        generateEntity()
        generateDao()
        generateRelationships()
        generateDbFile()
    }

    private fun generateEntity() {
        val generator = EntityGenerator()
        val typeConverterGenerator = TypeConverterGenerator()

        createdEntityList.forEach { entity ->
            generator.createEntityClass(entity, "generated")
            entity.fields.forEach { field ->
                if (field.fieldType.isListType()) {
                    typeConverterGenerator.execute(field.fieldType)
                }
            }
        }
    }

    private fun openEnterEntityName() {
        mainPanel.removeAll()
        val nameForm = EntityNameForm()
        val nameJPanel = nameForm.panel1
        nameJPanel.setBounds(10, 10, 580, 580)
        nameForm.setEnterEntityNameListener { name ->
            databaseName = name ?: ""
            openSelectDirectory()
        }

        mainPanel.add(nameJPanel)
    }


    private fun generateDao() {
        val generator = DaoFileGenerator()
        createdEntityList.forEach {
            if (it.generateDefaultQuery.queryType != AsyncQueryType.NOTHING) {
                generator.createDaoFile(it)
            }
        }
    }

    private fun generateRelationships() {
        val generator = RelationshipGenerator()

        relationshipList.forEach {
            generator.execute(it)
        }
    }

    private fun generateDbFile() {
        val generator = DbGenerator()

        val entityNameList = mutableListOf<String>()
        val daoNameList = mutableListOf<String>()

        createdEntityList.forEach { entityModel ->
            entityNameList.add(entityModel.entityName)
            if (entityModel.generateDefaultQuery.queryType != AsyncQueryType.NOTHING) {
                daoNameList.add(createDaoNameFromEntity(entityModel.entityName))
            }
        }

        generator.createDbFile(
            DbModel(
                dbName = "Test",
                entityNameList = entityNameList,
                daoNameList = daoNameList
            )
        )
    }

    private fun openInitializeVariantsForm() {
        mainPanel.removeAll()
        val initForm = InitializeVariantsForm()
        val initJPanel = initForm.rootPanel
        initJPanel.setBounds(10, 10, 580, 580)

        initForm.setCreateNewButtonClickListener {
            openEnterEntityName()
            openSelectDirectory()
        }

        initForm.setOpenExistButtonListener { path ->
            if (path != null) {
                analiseAndSaveToStorage(path)
                openSelectDirectory()
            }
        }

        mainPanel.add(initJPanel)
    }

    private fun openSelectDirectory() {
        mainPanel.removeAll()
        val selectForm = GenerateDirectoryName()
        val selectPanel = selectForm.panel
        selectPanel.setBounds(10, 10, 580, 580)

        selectForm.initComboBox()
        selectForm.setSelectPathListener {
            generatePath = it ?: ""
            openEntityListForm()
        }

        mainPanel.add(selectPanel)
    }

    private fun analiseAndSaveToStorage(path: String) {
        val entityModels = EntityParser().getEntities(path)
        createdEntityList.addAll(entityModels)
        databaseName = DbParser().defineExistDbName(path)
    }
}