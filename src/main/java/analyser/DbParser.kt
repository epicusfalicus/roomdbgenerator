package analyser

import java.io.File

class DbParser {

    fun defineExistDbName(path: String): String {
        try {
            val directory = File(path)
            val files = directory.listFiles()

            files.forEach { file ->
                getDatabaseNameOrNull(file.readLines())?.let {
                    return it
                }
            }
        } catch (e: ClassNotFoundException) {
            println("Error - $e")
            e.printStackTrace()
        }

        return ""
    }

    private fun getDatabaseNameOrNull(lines: List<String>): String? {
        return if (isLinesContainsDatabase(lines)) {
            val lineWithName = lines.find { line -> line.contains("abstract class") }
            lineWithName
                ?.replace("abstract class", "")
                ?.replace("\\s".toRegex(), "")
                ?.replace("(", "")
                ?.replace(")", "")
                ?.replace("RoomDatabase", "")
                ?.replace(":", "")
                ?: ""
        } else null
    }

    private fun isLinesContainsDatabase(lines: List<String>): Boolean {
        return lines.find { line -> line.contains("@Database") } != null
    }
}