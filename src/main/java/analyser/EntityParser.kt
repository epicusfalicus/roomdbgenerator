package analyser

import model.*
import java.io.File
import java.util.*

class EntityParser {

    fun getEntities(path: String): List<EntityModel> {
        return try {
            val parsedEntityWithPaths = parseEntities(path)
            return parseAndFillForeignKeys(parsedEntityWithPaths).map { it.second }
        } catch (e: ClassNotFoundException) {
            println("Error - $e")
            emptyList()
        }
    }

    private fun parseEntities(directoryPath: String): MutableList<Pair<List<String>, EntityModel>> {
        val entityWithFilePath = mutableListOf<Pair<List<String>, EntityModel>>()

        try {
            val directory = File(directoryPath)
            val files = directory.listFiles()

            files.forEach { file ->
                val fileLines = file.readLines()

                mapFileToEntityModel(fileLines)?.let { model ->
                    entityWithFilePath.add(fileLines to model)
                }
            }
        } catch (e: ClassNotFoundException) {
            println("Error - $e")
            e.printStackTrace()
        }

        return entityWithFilePath
    }

    private fun mapFileToEntityModel(lines: List<String>): EntityModel? {
        return if (isEntityFile(lines)) {
            EntityModel(
                entityName = getEntityName(lines),
                fields = getFieldsFromLines(lines)
            )
        } else {
            null
        }
    }

    private fun getEntityName(lines: List<String>): String {
        val lineWithName = lines.find { line -> line.contains("data class") }
        return lineWithName
            ?.replace("data class", "")
            ?.replace("\\s".toRegex(), "")
            ?.replace("(", "")
            ?.replace(")", "")
            ?: ""
    }

    private fun getFieldsFromLines(lines: List<String>): MutableList<FieldModel> {
        val fieldModels = mutableListOf<FieldModel>()

        val linesWithFields = lines.filter { line ->
            !line.contains("const") && (line.contains("val") || line.contains("var"))
        }
        val primaryKeyLine = lines.find { line -> line.contains("primaryKeys") }
        val primaryKey = primaryKeyLine
            ?.replace("\\s".toRegex(), "")
            ?.replace("primaryKeys", "")
            ?.replace("=", "")
            ?.replace("\"", "")
            ?.replace("/", "")
            ?.replace("[", "")
            ?.replace("]", "")
            ?.replace("(", "")
            ?.replace(")", "")


        linesWithFields.forEachIndexed { index, line ->
            val replacedLine = line
                .replace("\\s".toRegex(), "")
                .replace("val", "")
                .replace("var", "")
                .replace(",", "")

            val substrings = replacedLine.split(":")
            val fieldName =
                if (index > 0 && isContainsConstName(linesWithFields[index - 1])) {
                    getConstNameField(linesWithFields[index - 1])
                } else substrings[0]

             val fieldType = substrings[1].toFieldType()

            val fieldModel = FieldModel(
                fieldName = fieldName,
                fieldType = fieldType,
                primaryKey = primaryKey.equals(fieldName)
            )

            fieldModels.add(fieldModel)
        }

        return fieldModels
    }

    private fun isEntityFile(lines: List<String>): Boolean {
        lines.forEach { line ->
            if (line.contains("@Entity")) return true
        }

        return false
    }

    private fun isContainsConstName(line: String): Boolean {
        return line.contains("@ColumnInfo")
    }

    private fun getConstNameField(line: String): String {
        return line
            .replace("\\s".toRegex(), "")
            .replace("@ColumnInfo", "")
            .replace("=", "")
            .replace("name", "")
            .replace("(", "")
            .replace(")", "")
            .replace("/", "")
    }

    private fun parseAndFillForeignKeys(pathsWithEntities: List<Pair<List<String>, EntityModel>>): List<Pair<List<String>, EntityModel>> {
        val allEntityModels = pathsWithEntities.map { it.second }

        pathsWithEntities.forEach { pathWithEntity ->
            if (isContainsForeignKey(pathWithEntity.first)) {
                pathWithEntity.second.foreignKeyModel = getForeignKeyFromLines(
                    lines = pathWithEntity.first,
                    entities = allEntityModels,
                    currentEntity = pathWithEntity.second
                )
            }
        }

        return pathsWithEntities
    }

    private fun isContainsForeignKey(lines: List<String>): Boolean {
        lines.forEach { line ->
            if (line.contains("foreignKeys")) return true
        }

        return false
    }

    private fun getForeignKeyFromLines(
        lines: List<String>,
        entities: List<EntityModel>,
        currentEntity: EntityModel
    ): ForeignKeyModel? {
        val parentTable = getParentTableName(lines, entities) ?: return null
        val parentColumn = getParentColumn(lines, parentTable) ?: return null
        val childColumn = getChildColumn(lines, currentEntity) ?: return null
        val updateStrategy = getStrategyType("onUpdate", lines)
        val deleteStrategy = getStrategyType("onDelete", lines)

        return ForeignKeyModel(
            foreignKeyName = "foreignKey ${Random().nextInt()}",
            parentTable = parentTable,
            column = childColumn,
            parentColumn = parentColumn,
            onUpdate = updateStrategy,
            onDelete = deleteStrategy
        )
    }

    private fun getParentTableName(lines: List<String>, entities: List<EntityModel>): EntityModel? {
        val parentTableLine = lines.first { line -> line.contains("entity =") }
        val parentTableStringName = parentTableLine
            .replace("entity =", "")
            .replace("\\s".toRegex(), "")
            .replace("::class", "")
            .replace(",", "")
            .replace("/", "")

        return entities.find { entity ->
            entity.entityName == parentTableStringName
        }
    }

    private fun getParentColumn(lines: List<String>, parentEntity: EntityModel): FieldModel? {
        val parentColumnLine = lines.first { line -> line.contains("parentColumns =") }
        val parentColumnCapsStringName = parentColumnLine
            .replace("parentColumns =", "")
            .replace("\\s".toRegex(), "")
            .replace("arrayOf", "")
            .replace(",", "")
            .replace("(", "")
            .replace(")", "")
            .replace("/", "")
            .replace(parentEntity.entityName, "")
            .replace(".", "")

        return parentEntity.fields.find { fieldModel ->
            fieldModel.fieldName.uppercase(Locale.getDefault()) == parentColumnCapsStringName
        }
    }

    private fun getChildColumn(lines: List<String>, childEntity: EntityModel): FieldModel? {
        val childColumnLine = lines.first { line -> line.contains("childColumns =") }
        val childColumnCapsStringName = childColumnLine
            .replace("childColumns =", "")
            .replace("\\s".toRegex(), "")
            .replace("arrayOf", "")
            .replace(",", "")
            .replace("(", "")
            .replace(")", "")
            .replace("/", "")
            .replace(childEntity.entityName, "")
            .replace(".", "")

        return childEntity.fields.find { fieldModel ->
            fieldModel.fieldName.uppercase(Locale.getDefault()) == childColumnCapsStringName
        }
    }

    private fun getStrategyType(strategyType: String, lines: List<String>): UpdateStrategy {
        val lineWithStrategy = lines.first { line -> line.contains(strategyType) }
        val strategyNameString = lineWithStrategy
            .replace("$strategyType =", "")
            .replace("\\s".toRegex(), "")
            .replace("ForeignKey.", "")
            .replace(",", "")
            .replace("/", "")

        return UpdateStrategy.valueOf(strategyNameString)
    }
}