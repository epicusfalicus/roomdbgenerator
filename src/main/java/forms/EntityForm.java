package forms;

import com.intellij.ui.components.JBList;
import model.*;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class EntityForm {
    private JPanel rootJPanel;
    private JLabel entityNameJLabel;
    private JTable entityContentJTable;
    private JLabel columnName2JLabel;
    private JLabel dataTypeJLabel;
    private JLabel defaultValueJLabel;
    private JLabel primaryKeyJLabel;
    private JLabel autoIncrementJLabel;
    private JTextField defaultValueTextField;
    private JLabel nullableJLabel;
    private JLabel generateQueryJLabel;
    private JCheckBox primaryKeyCheckBox;
    private JCheckBox autoIncrementCheckBox;
    private JCheckBox nullableCheckBox;
    private JCheckBox generateQueryCheckBox;
    private JTextField columnNameTextField;
    private JButton columnsButton;
    private JButton foreignKeysButton;
    private JScrollPane scrollPane;
    private JPanel jpane;
    private JBList<String> columnsList;
    private JButton addMoreButton;
    private JPanel columnsLabel;
    private JButton saveEntityButton;
    private JButton saveColumnButton;
    private JTextField entityNameTextField;
    private JSpinner queryTypeSpinner;
    private JButton relationshipButton;
    private JComboBox dataTypeComboBox;

    private DefaultListModel<String> columnsListDefaultModel;
    private EntityModel entityModel;

    public EntityForm(EntityModel model) {
        entityModel = model;

        List<String> elements = model.getFields().stream()
                .map(FieldModel::getFieldName)
                .collect(Collectors.toList());

        columnsListDefaultModel.addAll(elements);
        entityNameTextField.setText(model.getEntityName());

        initAddMoreButtonListener();
        initSpinnerModel();
    }

    public EntityModel getEntityModel() {
        return entityModel;
    }

    public String getEntityName() {
        return entityNameTextField.getText();
    }

    public JPanel getRootJPanel() {
        return rootJPanel;
    }

    public JLabel getEntityNameJLabel() {
        return entityNameJLabel;
    }

    public JTable getEntityContentJTable() {
        return entityContentJTable;
    }

    public JLabel getColumnName2JLabel() {
        return columnName2JLabel;
    }

    public JLabel getDataTypeJLabel() {
        return dataTypeJLabel;
    }

    public JLabel getDefaultValueJLabel() {
        return defaultValueJLabel;
    }

    public JLabel getPrimaryKeyJLabel() {
        return primaryKeyJLabel;
    }

    public JLabel getAutoIncrementJLabel() {
        return autoIncrementJLabel;
    }

    public JTextField getDefaultValueTextField() {
        return defaultValueTextField;
    }

    public JLabel getNullableJLabel() {
        return nullableJLabel;
    }

    public JLabel getGenerateQueryJLabel() {
        return generateQueryJLabel;
    }

    public JCheckBox getPrimaryKeyCheckBox() {
        return primaryKeyCheckBox;
    }

    public JCheckBox getAutoIncrementCheckBox() {
        return autoIncrementCheckBox;
    }

    public JCheckBox getNullableCheckBox() {
        return nullableCheckBox;
    }

    public JCheckBox getGenerateQueryCheckBox() {
        return generateQueryCheckBox;
    }

    public JTextField getColumnNameTextField() {
        return columnNameTextField;
    }

    public JButton getColumnsButton() {
        return columnsButton;
    }

    public JButton getForeignKeysButton() {
        return foreignKeysButton;
    }

    public void setSaveEntityButtonClickListener(ActionListener saveEntityListener) {
        saveEntityButton.addActionListener(saveEntityListener);
    }

    public void setForeignKeyButtonClickListener(ActionListener saveEntityListener) {
        foreignKeysButton.addActionListener(saveEntityListener);
    }

    public void setRelationshipButtonClickListener(ActionListener saveEntityListener) {
        relationshipButton.addActionListener(saveEntityListener);
    }

    private void initAddMoreButtonListener() {
        ActionListener addMoreAction = e -> {
            FieldModel newField = new FieldModel();
            newField.setFieldName("newField" + (columnsListDefaultModel.size() + 1));
            columnsListDefaultModel.addElement(newField.getFieldName());
            entityModel.getFields().add(newField);
        };

        addMoreButton.addActionListener(addMoreAction);
    }

    private void onFieldSelected(FieldModel model) {
        columnNameTextField.setText(model.getFieldName());
        primaryKeyCheckBox.setSelected(model.getPrimaryKey());
        autoIncrementCheckBox.setSelected(model.getNeedAutoincrement());
        nullableCheckBox.setSelected(model.getNullable());
        defaultValueTextField.setText(model.getDefaultValue());

        dataTypeComboBox.addItemListener(null);
        if (model.getFieldType() instanceof FieldType.LIST_CUSTOM) {
            dataTypeComboBox.setSelectedItem("Custom");
        } else {
            dataTypeComboBox.setSelectedItem(model.getFieldType().getValue());
        }
//        initDataTypeChangeListener();

        setSaveColumnButtonClickListener(null);
        setSaveColumnButtonClickListener(model);
    }

    private void setSaveColumnButtonClickListener(FieldModel model) {
        ActionListener saveColumnListener = e -> {
            int index = columnsListDefaultModel.indexOf(model.getFieldName());

            model.setFieldName(columnNameTextField.getText());
            model.setPrimaryKey(primaryKeyCheckBox.isSelected());

            if (dataTypeComboBox.getSelectedItem() != null) {
                FieldType ft;

                switch (dataTypeComboBox.getSelectedItem().toString()) {
                    case ("String"): {
                        ft = FieldType.STRING.INSTANCE;
                        break;
                    }
                    case ("Boolean"): {
                        ft = FieldType.BOOLEAN.INSTANCE;
                        break;
                    }
                    case ("Int"): {
                        ft = FieldType.INT.INSTANCE;
                        break;
                    }
                    case ("List<String>"): {
                        ft = FieldType.LIST_STRING.INSTANCE;
                        break;
                    }
                    case ("List<Boolean>"): {
                        ft = FieldType.LIST_BOOLEAN.INSTANCE;
                        break;
                    }
                    case ("List<Int>"): {
                        ft = FieldType.LIST_INT.INSTANCE;
                        break;
                    }
                    default:
                        ft = new FieldType.LIST_CUSTOM(dataTypeComboBox.getSelectedItem().toString());
                        break;
                }
                model.setFieldType(ft);
            }

            model.setNeedAutoincrement(autoIncrementCheckBox.isSelected());
            model.setNullable(nullableCheckBox.isSelected());
            model.setDefaultValue(defaultValueTextField.getText());
            columnsListDefaultModel.set(index, columnNameTextField.getText());
        };

        saveColumnButton.addActionListener(saveColumnListener);
    }

    private void createUIComponents() {
        columnsListDefaultModel = new DefaultListModel<>();
        columnsList = new JBList<>(columnsListDefaultModel);
        columnsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        ListSelectionListener selectionListener = e -> {
            int selectedIndex = ((JBList<?>) e.getSource()).getSelectedIndex();
            onFieldSelected(entityModel.getFields().get(selectedIndex));
        };
        columnsList.addListSelectionListener(selectionListener);

        initQuerySpinnerModel();
    }

    private void initSpinnerModel() {
        String[] valuesArray = new String[]{
                FieldType.STRING.INSTANCE.getValue(),
                FieldType.BOOLEAN.INSTANCE.getValue(),
                FieldType.INT.INSTANCE.getValue(),
                FieldType.LIST_STRING.INSTANCE.getValue(),
                FieldType.LIST_BOOLEAN.INSTANCE.getValue(),
                FieldType.LIST_INT.INSTANCE.getValue(),
                new FieldType.LIST_CUSTOM().getCustomValue(),
        };
        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(valuesArray);
        dataTypeComboBox.setModel(model);
        dataTypeComboBox.setSelectedItem(null);
        initDataTypeChangeListener();
    }

    private void initDataTypeChangeListener() {
        dataTypeComboBox.addItemListener(e -> {

            if (e.getItem() == "List<Custom>") {
                JFileChooser chooser = new JFileChooser();
                int result = chooser.showOpenDialog(rootJPanel);
                if (result == JFileChooser.APPROVE_OPTION) {
                    String selectedItem = chooser.getSelectedFile().toString();
                    dataTypeComboBox.addItem(selectedItem);
                    dataTypeComboBox.setSelectedItem(selectedItem);
                }
            }
        });
    }

    private void initQuerySpinnerModel() {
        List<String> values = Arrays.stream(AsyncQueryType.values()).map(AsyncQueryType::getAsyncName).collect(Collectors.toList());
        SpinnerModel model = new SpinnerListModel(values);
        queryTypeSpinner = new JSpinner(model);
        queryTypeSpinner.setValue(entityModel.getGenerateDefaultQuery().getQueryType().getAsyncName());
        queryTypeSpinner.addChangeListener(e -> getEntityModel().setGenerateDefaultQuery(
                Arrays.stream(AsyncQueryType.values())
                        .filter(value -> value.getAsyncName().equals(queryTypeSpinner.getValue()))
                        .findFirst()
                        .map(QueryModel::new)
                        .orElse(new QueryModel(AsyncQueryType.NOTHING))
        ));
    }
}