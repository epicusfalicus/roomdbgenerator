package forms;

import common.StringCallback;

import javax.swing.*;

public class EntityNameForm {
    private JTextField dbNameField;
    private JPanel panel;
    private JButton enterButton;

    public JPanel getPanel1() {
        return panel;
    }

    public void setEnterEntityNameListener(StringCallback callback) {
        enterButton.addActionListener(e -> callback.stringCallback(dbNameField.getText()));
    }
}
