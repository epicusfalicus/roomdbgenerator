package forms;

import model.*;
import storage.Storage;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;

public class ForeignKeyForm {
    private JLabel keyNameLabel;
    private JLabel parentTableLabel;
    private JLabel columnLabel;
    private JLabel parentColumnLabel;
    private JLabel onUpdateLabel;
    private JLabel onDeleteLabel;
    private JTextField foreignKeyNameTextField;
    private JComboBox parentTableComboBox;
    private JComboBox columnComboBox;
    private JComboBox parentColumnComboBox;
    private JComboBox onUpdateComboBox;
    private JComboBox onDeleteComboBox;
    private JButton saveForeignKeyButton;
    private JButton columnsButton;
    private JPanel foreignKeyJPanel;

    private ForeignKeyModel foreignKeyModel = new ForeignKeyModel();
    private EntityModel currentEntityModel;

    public EntityModel getCurrentEntityModel() {
        return currentEntityModel;
    }

    public ForeignKeyModel getForeignKeyModel() {
        return foreignKeyModel;
    }

    public JPanel getForeignKeyJPanel() {
        return foreignKeyJPanel;
    }

    public void setForeignKeyModel(ForeignKeyModel foreignKeyModel) {
        this.foreignKeyModel = foreignKeyModel;
    }

    public JButton getSaveForeignKeyButton() {
        return saveForeignKeyButton;
    }

    public ForeignKeyForm(EntityModel entityModel) {
        currentEntityModel = entityModel;

        if (entityModel.getForeignKeyModel() != null) {
            foreignKeyModel = entityModel.getForeignKeyModel();
        }

        initReferenceTableBox();
        initColumnList(currentEntityModel.getFields());
        initStrategyBoxes(foreignKeyModel);

        if (!foreignKeyModel.getForeignKeyName().isEmpty()) {
            foreignKeyNameTextField.setText(foreignKeyModel.getForeignKeyName());
        }

        if (foreignKeyModel.getParentTable() != null) {
            initReferenceColumnList(foreignKeyModel.getParentTable().getFields());
            setParentTableSelection(foreignKeyModel.getParentTable());
        }

        if (foreignKeyModel.getParentColumn() != null && !foreignKeyModel.getParentColumn().getFieldName().isEmpty()) {
            setReferenceColumnsSelection(foreignKeyModel.getParentColumn());
        }

        if (foreignKeyModel.getColumn() != null && !foreignKeyModel.getColumn().getFieldName().isEmpty()) {
            setColumnsSelection(foreignKeyModel.getColumn());
        }

        initSaveButtonListener();
    }

    public void setColumnsButtonListener(ActionListener listener) {
        columnsButton.addActionListener(listener);
    }

    private void initReferenceTableBox() {
        parentTableComboBox.removeAllItems();
        String[] tables = Storage.INSTANCE.getCreatedEntityList().stream()
                .map(EntityModel::getEntityName)
                .filter(value -> !value.equals(currentEntityModel.getEntityName()))
                .toArray(String[]::new);

        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(tables);

        parentTableComboBox.setModel(model);
        parentColumnComboBox.setSelectedItem(null);

        parentTableComboBox.addItemListener(e -> {
            EntityModel selectedEntity = Storage.INSTANCE.getCreatedEntityList().stream()
                    .filter(value -> value.getEntityName().equals(e.getItem()))
                    .findFirst()
                    .orElse(new EntityModel());

            initReferenceColumnList(selectedEntity.getFields());
        });
    }

    private void initReferenceColumnList(List<FieldModel> fieldsModel) {
        parentColumnComboBox.removeAllItems();
        String[] fieldsName = fieldsModel.stream()
                .map(FieldModel::getFieldName)
                .toArray(String[]::new);

        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(fieldsName);

        parentColumnComboBox.setModel(model);
    }

    private void initColumnList(List<FieldModel> fieldsModel) {
        columnComboBox.removeAllItems();
        String[] fieldsName = fieldsModel.stream()
                .map(FieldModel::getFieldName)
                .toArray(String[]::new);

        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(fieldsName);
        columnComboBox.setModel(model);
        columnComboBox.setSelectedItem(null);
    }

    private void setParentTableSelection(EntityModel model) {
        parentTableComboBox.setSelectedItem(model.getEntityName());
    }

    private void setReferenceColumnsSelection(FieldModel fieldModel) {
        parentColumnComboBox.setSelectedItem(fieldModel.getFieldName());
    }

    private void setColumnsSelection(FieldModel fieldModel) {
        columnComboBox.setSelectedItem(fieldModel.getFieldName());
    }

    private void initStrategyBoxes(ForeignKeyModel foreignKeyModel) {
        String[] values = Arrays.stream(UpdateStrategy.values()).map(UpdateStrategy::getValue).toArray(String[]::new);
        DefaultComboBoxModel<String> onUpdateComboBoxModel = new DefaultComboBoxModel<>(values);
        DefaultComboBoxModel<String> onDeleteComboBoxModel = new DefaultComboBoxModel<>(values);
        onUpdateComboBox.setModel(onUpdateComboBoxModel);
        onDeleteComboBox.setModel(onDeleteComboBoxModel);
        onUpdateComboBox.setSelectedItem(foreignKeyModel.getOnUpdate().getValue());
        onDeleteComboBox.setSelectedItem(foreignKeyModel.getOnDelete().getValue());
    }

    private void initSaveButtonListener() {
        saveForeignKeyButton.addActionListener(e -> {
            foreignKeyModel.setForeignKeyName(foreignKeyNameTextField.getText());

            if (parentTableComboBox.getSelectedItem() != null) {
                EntityModel referenceTableModel = Storage.INSTANCE.getCreatedEntityList().stream()
                        .filter(value -> value.getEntityName().equals(parentTableComboBox.getSelectedItem()))
                        .findFirst()
                        .orElse(null);

                foreignKeyModel.setParentTable(referenceTableModel);

                FieldModel referenceFieldModel = referenceTableModel.getFields().stream()
                        .filter(value -> value.getFieldName().equals(parentColumnComboBox.getSelectedItem()))
                        .findFirst()
                        .orElse(null);

                foreignKeyModel.setParentColumn(referenceFieldModel);
            }

            FieldModel columnFieldModel = currentEntityModel.getFields().stream()
                    .filter(value -> value.getFieldName().equals(columnComboBox.getSelectedItem()))
                    .findFirst()
                    .orElse(null);

            foreignKeyModel.setColumn(columnFieldModel);

            foreignKeyModel.setOnUpdate(
                    Arrays.stream(UpdateStrategy.values())
                            .filter(value -> value.getValue().equals(onUpdateComboBox.getSelectedItem()))
                            .findFirst()
                            .orElse(UpdateStrategy.RESTRICT)
            );

            foreignKeyModel.setOnDelete(
                    Arrays.stream(UpdateStrategy.values())
                            .filter(value -> value.getValue().equals(onDeleteComboBox.getSelectedItem()))
                            .findFirst()
                            .orElse(UpdateStrategy.RESTRICT)
            );

            currentEntityModel.setForeignKeyModel(foreignKeyModel);

            Storage.INSTANCE.getCreatedEntityList().stream()
                    .filter(value -> value.getEntityName().equals(currentEntityModel.getEntityName()))
                    .findFirst()
                    .orElse(null);
        });
    }
}