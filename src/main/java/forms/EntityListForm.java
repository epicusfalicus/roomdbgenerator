package forms;

import com.intellij.ui.components.JBList;
import model.EntityModel;
import storage.Storage;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.stream.Collectors;

public class EntityListForm {

    private DefaultListModel<String> entityListDefaultModel;

    public EntityListForm(List<EntityModel> entityModelList) {
        List<String> elements = entityModelList.stream()
                .map(EntityModel::getEntityName)
                .collect(Collectors.toList());

        entityListDefaultModel.addAll(elements);
    }

    public JPanel getEntityListPanel() {
        return entityListPanel;
    }

    public JList<String> getEntityList() {
        return entityList;
    }

    public JScrollPane getEntityListScrollPane() {
        return entityListScrollPane;
    }

    public EntityModel getCurrentSelectedEntityModel() {
        List<EntityModel> index = Storage.INSTANCE.getCreatedEntityList().stream()
                .filter(entityModel -> entityModel.getEntityName().equals(entityList.getSelectedValue()))
                .collect(Collectors.toList());

        return index.get(0);
    }

    public void setOpenButtonClickListener(ActionListener listener) {
        openEntityButton.addActionListener(listener);
    }

    public void setCreateButtonClickListener(ActionListener listener) {
        createButton.addActionListener(listener);
    }

    public void setGenerateButtonClickListener(ActionListener listener) {
        generateButton.addActionListener(listener);
    }

    JPanel entityListPanel;
    JList<String> entityList;
    JScrollPane entityListScrollPane;
    private JButton openEntityButton;
    private JButton createButton;
    private JButton generateButton;

    private void createUIComponents() {
        entityListDefaultModel = new DefaultListModel<>();
        entityList = new JBList<>(entityListDefaultModel);
        entityList.setBounds(10, 10, 600, 600);
        entityList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
}
