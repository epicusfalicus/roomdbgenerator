package forms;

import common.StringCallback;

import javax.swing.*;

public class GenerateDirectoryName {
    private JButton selectButton;
    private JComboBox comboBox1;
    private JPanel panel;

    public JPanel getPanel() {
        return panel;
    }

    public void setSelectPathListener(StringCallback callback) {
        selectButton.addActionListener(e -> callback.stringCallback(comboBox1.getSelectedItem().toString()));
    }

    public void initComboBox() {
        String[] valuesArray = new String[]{"Select directory"};
        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(valuesArray);
        comboBox1.setModel(model);
        comboBox1.setSelectedItem(null);
        comboBox1.addItemListener(e -> {

            if (e.getItem() != null && e.getItem() == "Select directory") {
                JFileChooser chooser = new JFileChooser();
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int result = chooser.showOpenDialog(panel);
                if (result == JFileChooser.APPROVE_OPTION) {
                    String selectedItem = chooser.getSelectedFile().toString();
                    comboBox1.addItem(selectedItem);
                    comboBox1.setSelectedItem(selectedItem);
                }
            }
        });
    }
}
