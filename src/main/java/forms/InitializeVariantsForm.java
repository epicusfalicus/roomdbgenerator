package forms;

import common.StringCallback;

import javax.swing.*;
import java.awt.event.ActionListener;

public class InitializeVariantsForm {
    private JPanel rootPanel;
    private JButton createNewButton;
    private JButton openExistButton;

    public JPanel getRootPanel() {
        return rootPanel;
    }

    public InitializeVariantsForm() {

    }

    public void setCreateNewButtonClickListener(ActionListener listener) {
        createNewButton.addActionListener(listener);
    }

    public void setOpenExistButtonListener(StringCallback callback) {
        openExistButton.addActionListener(action -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

            int result = chooser.showOpenDialog(rootPanel);

            if (result == JFileChooser.APPROVE_OPTION) {
                callback.stringCallback(chooser.getSelectedFile().toString());
            }
        });
    }
}