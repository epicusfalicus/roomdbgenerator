package forms;

import com.intellij.ui.components.JBList;
import model.*;
import storage.Storage;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RelationshipForm {
    private JPanel rootJPanel;
    private JLabel relLabel;
    private JPanel relListJPanel;
    private JScrollPane relScrollPane;
    private JButton addMoreButton;
    private JList relList;
    private JLabel linkedTableLabel;
    private JComboBox linkedTableComboBox;
    private JComboBox linkedColumnComboBox;
    private JComboBox columnComboBox;
    private JComboBox relTypeComboBox;
    private JLabel parentColumnLabel;
    private JLabel columnLabel;
    private JLabel relTypeLabel;
    private JPanel buttonsPanel;
    private JButton saveButton;
    private JButton columnsButton;
    private JLabel relNameLabel;
    private JTextField relNameTextField;

    private DefaultListModel<String> relListDefaultModel;
    private List<RelationshipModel> existRelList;
    private EntityModel currentEntityModel;
    private List<EntityModel> entityModelsWithoutCurrent;

    public JPanel getRootJPanel() {
        return rootJPanel;
    }

    public EntityModel getCurrentEntityModel() {
        return currentEntityModel;
    }

    public RelationshipForm(EntityModel model) {
        currentEntityModel = model;
        inflateRelList(model);
        inflateLinkedTableBoxList();
        initLinkedBoxSelectListener();
        initAddMoreButtonListener();

    }

    private void inflateRelList(EntityModel model) {
        existRelList = Storage.INSTANCE.getRelationshipList().stream()
                .filter(element -> element.getParentTable() != null && element.getChildTable() != null)
                .filter(element -> element.getChildTable().getEntityName().equals(model.getEntityName()) ||
                        element.getParentTable().getEntityName().equals(model.getEntityName()))
                .collect(Collectors.toList());

        relListDefaultModel.addAll(
                existRelList.stream().map(RelationshipModel::getRelationshipName)
                        .collect(Collectors.toList())
        );
    }

    private void inflateLinkedTableBoxList() {
        entityModelsWithoutCurrent = Storage.INSTANCE.getCreatedEntityList().stream()
                .filter(element -> !element.getEntityName().equals(currentEntityModel.getEntityName()))
                .collect(Collectors.toList());
    }

    private void createUIComponents() {
        initRelationshipsList();
    }

    private void initRelationshipsList() {
        relListDefaultModel = new DefaultListModel<>();
        relList = new JBList<>(relListDefaultModel);
        relList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        ListSelectionListener selectionListener = e -> {
            int selectedIndex = ((JBList<?>) e.getSource()).getSelectedIndex();
            onRelSelected(existRelList.get(selectedIndex));
        };
        relList.addListSelectionListener(selectionListener);
    }

    private void onRelSelected(RelationshipModel relModel) {
        EntityModel curEntityModel = currentEntityModel;
        EntityModel linkedEntityModel = null;
        FieldModel column = null;
        FieldModel linkedColumn = null;
        relNameTextField.setText(relModel.getRelationshipName());

        if (relModel.getParentTable() != null && relModel.getChildTable() != null) {

            if (relModel.getParentTable().getEntityName().equals(currentEntityModel.getEntityName())) {
                curEntityModel = relModel.getParentTable();
                linkedEntityModel = relModel.getChildTable();
                column = relModel.getParentColumn();
                linkedColumn = relModel.getChildColumn();
            }
            if (relModel.getChildTable().getEntityName().equals(currentEntityModel.getEntityName())) {
                curEntityModel = relModel.getChildTable();
                linkedEntityModel = relModel.getParentTable();
                column = relModel.getChildColumn();
                linkedColumn = relModel.getParentColumn();
            }
        }

        initLinkedTableBoxList(linkedEntityModel);
        initColumnBoxList(curEntityModel.getFields(), column);

        if (linkedEntityModel != null) {
            initLinkedColumnBoxList(linkedEntityModel.getFields(), linkedColumn);
        }

        initRelTypeBox(relModel);
        setSaveButtonListener(relModel);
    }

    private void initLinkedTableBoxList(EntityModel linkedEntityModel) {
        linkedTableComboBox.removeAllItems();
        String[] fieldsName = entityModelsWithoutCurrent.stream()
                .map(EntityModel::getEntityName)
                .toArray(String[]::new);

        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(fieldsName);
        linkedTableComboBox.setModel(model);

        if (linkedEntityModel != null) {
            linkedTableComboBox.setSelectedItem(linkedEntityModel.getEntityName());
        } else {
            linkedTableComboBox.setSelectedItem(null);
        }
    }

    private void initLinkedBoxSelectListener() {
        linkedTableComboBox.addItemListener(e -> {
            EntityModel selectedEntity = Storage.INSTANCE.getCreatedEntityList().stream()
                    .filter(value -> value.getEntityName().equals(e.getItem()))
                    .findFirst()
                    .orElse(new EntityModel());

            initLinkedColumnBoxList(selectedEntity.getFields(), null);
        });
    }

    private void initColumnBoxList(List<FieldModel> fieldsModel, FieldModel selectedField) {
        columnComboBox.removeAllItems();
        String[] fieldsName = fieldsModel.stream()
                .map(FieldModel::getFieldName)
                .toArray(String[]::new);

        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(fieldsName);
        columnComboBox.setModel(model);

        if (selectedField != null) {
            columnComboBox.setSelectedItem(selectedField.getFieldName());
        } else {
            columnComboBox.setSelectedItem(null);
        }
    }

    private void initLinkedColumnBoxList(List<FieldModel> fieldsModel, FieldModel selectedField) {
        linkedColumnComboBox.removeAllItems();
        String[] fieldsName = fieldsModel.stream()
                .map(FieldModel::getFieldName)
                .toArray(String[]::new);

        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(fieldsName);
        linkedColumnComboBox.setModel(model);

        if (selectedField != null) {
            linkedColumnComboBox.setSelectedItem(selectedField.getFieldName());
        } else {
            linkedColumnComboBox.setSelectedItem(null);
        }
    }

    private void initRelTypeBox(RelationshipModel relationshipModel) {
        // TODO add type selection before
        String[] values = Arrays.stream(RelationshipType.values()).map(RelationshipType::getValue).toArray(String[]::new);
        DefaultComboBoxModel<String> relaTypeComboBoxModel = new DefaultComboBoxModel<>(values);
        relTypeComboBox.setModel(relaTypeComboBoxModel);

        if (relationshipModel.getRelationshipType() != null) {
            relTypeComboBox.setSelectedItem(relationshipModel.getRelationshipType().getValue());
        } else {
            relTypeComboBox.setSelectedItem(null);
        }
    }

    private void initAddMoreButtonListener() {
        ActionListener addMoreAction = e -> {
            RelationshipModel newRel = new RelationshipModel();
            newRel.setRelationshipName("newRelationship" + relListDefaultModel.size());
            relListDefaultModel.addElement(newRel.getRelationshipName());
            existRelList.add(newRel);
        };

        addMoreButton.addActionListener(addMoreAction);
    }

    public void setColumnsButtonListener(ActionListener listener) {
        columnsButton.addActionListener(listener);
    }

    private void setSaveButtonListener(RelationshipModel relModel) {
        saveButton.addActionListener(e -> {
            saveTableWithColumns(relModel);
            saveRelType(relModel);
            switchRelFromStorage(relModel);
            relModel.setRelationshipName(relNameTextField.getText());
        });
    }

    private void saveTableWithColumns(RelationshipModel relModel) {
        EntityModel linkedTableModel = Storage.INSTANCE.getCreatedEntityList().stream()
                .filter(value -> value.getEntityName().equals(linkedTableComboBox.getSelectedItem()))
                .findFirst()
                .orElse(null);

        if (linkedTableModel != null) {

            FieldModel linkedTableColumn = linkedTableModel.getFields().stream()
                    .filter(value -> value.getFieldName().equals(linkedColumnComboBox.getSelectedItem()))
                    .findFirst()
                    .orElse(null);

            FieldModel parentTableColumn = currentEntityModel.getFields().stream()
                    .filter(value -> value.getFieldName().equals(columnComboBox.getSelectedItem()))
                    .findFirst()
                    .orElse(null);

            if (relModel.getParentTable() != null && relModel.getParentTable().getEntityName().equals(currentEntityModel.getEntityName())) {
                relModel.setChildTable(linkedTableModel);
                relModel.setChildColumn(linkedTableColumn);
                relModel.setParentColumn(parentTableColumn);
            } else {
                if (relModel.getParentTable() == null) {
                    relModel.setChildTable(linkedTableModel);
                    relModel.setChildColumn(linkedTableColumn);
                    relModel.setParentColumn(parentTableColumn);
                    relModel.setParentTable(currentEntityModel);
                } else {
                    relModel.setParentTable(linkedTableModel);
                    relModel.setParentColumn(linkedTableColumn);
                    relModel.setChildColumn(parentTableColumn);
                }
            }
        }
    }
    private void saveRelType(RelationshipModel relModel) {
        relModel.setRelationshipType(
                Arrays.stream(RelationshipType.values())
                        .filter(value -> value.getValue().equals(relTypeComboBox.getSelectedItem()))
                        .findFirst()
                        .orElse(RelationshipType.ONE_TO_ONE)
        );
    }

    private void switchRelFromStorage(RelationshipModel relModel) {
        List<RelationshipModel> relList = Storage.INSTANCE.getRelationshipList();

        RelationshipModel changedRel = relList.stream()
                .filter(value -> value.getRelationshipName().equals(relModel.getRelationshipName()))
                .findFirst()
                .orElse(null);

        int elIndex = relList.indexOf(changedRel);

        if (elIndex != -1) {
            relList.remove(elIndex);
            relList.add(elIndex, relModel);
        } else {
            relList.add(relModel);
        }
    }
}

