package model

sealed class FieldType(val value: String) {
    object STRING : FieldType("String")
    object INT : FieldType("Int")
    object BOOLEAN : FieldType("Boolean")
    object LIST_STRING : FieldType("List<String>")
    object LIST_INT : FieldType("List<Int>")
    object LIST_BOOLEAN : FieldType("List<Boolean>")
    class LIST_CUSTOM(var customValue: String = "List<Custom>") : FieldType(customValue)
}

fun FieldType.isListType(): Boolean {
    return when (this) {
        is FieldType.LIST_BOOLEAN, is FieldType.LIST_INT, is FieldType.LIST_STRING, is FieldType.LIST_CUSTOM -> true
        else -> false
    }
}

fun String.toFieldType(): FieldType {
    return when (this) {
        "String" -> FieldType.STRING
        "Int" -> FieldType.STRING
        "Boolean" -> FieldType.BOOLEAN
        "List<String>" -> FieldType.LIST_STRING
        "List<Int>" -> FieldType.LIST_INT
        "List<Boolean>" -> FieldType.LIST_BOOLEAN
        else -> FieldType.LIST_CUSTOM(this)
    }
}