package model

data class DbModel(
    var dbName: String?,
    val entityNameList: MutableList<String>,
    val daoNameList: MutableList<String>
)