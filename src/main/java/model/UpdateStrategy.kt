package model

enum class UpdateStrategy(val value: String) {
    CASCADE("CASCADE"), SET_NULL("SET_NULL"), RESTRICT("RESTRICT"), SET_DEFAULT("SET_DEFAULT")
}