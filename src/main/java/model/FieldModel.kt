package model

data class FieldModel(
    var fieldName: String = "",
    var fieldType: FieldType = FieldType.STRING,
    var primaryKey: Boolean = false,
    var needAutoincrement: Boolean = false,
    var defaultValue: String? = null,
    var nullable: Boolean = false,
)