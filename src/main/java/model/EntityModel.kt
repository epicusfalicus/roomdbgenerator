package model

data class EntityModel(
    var entityName: String = "",
    val fields: MutableList<FieldModel> = mutableListOf(),
    var foreignKeyModel: ForeignKeyModel? = null,
    var generateDefaultQuery: QueryModel = QueryModel(AsyncQueryType.NOTHING)
)