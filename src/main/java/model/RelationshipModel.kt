package model

data class RelationshipModel(
    var relationshipName: String = "",
    var childTable: EntityModel? = null,
    var parentTable: EntityModel? = null,
    var childColumn: FieldModel? = null,
    var parentColumn: FieldModel? = null,
    var relationshipType: RelationshipType? = null
)