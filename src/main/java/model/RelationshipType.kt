package model

enum class RelationshipType(val value: String) {
    ONE_TO_ONE("One to one"),
    ONE_TO_MANY("One to many"),
    MANY_TO_ONE("Many to one"),
    MANY_TO_MANY("Many to many")
}