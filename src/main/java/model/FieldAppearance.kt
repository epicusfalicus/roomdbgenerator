package model

data class FieldAppearance(
    val fieldModel: FieldModel,
    val fieldNameCaps: String,
    val fieldNameLower: String,
    val typeText: String,
)