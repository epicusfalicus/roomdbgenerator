package model

enum class AsyncQueryType(val asyncName: String) {
    NOTHING("No"), RXJAVA("RxJava"), COROUTINES("Coroutines")
}