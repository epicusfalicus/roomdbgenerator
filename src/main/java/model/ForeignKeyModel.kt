package model

data class ForeignKeyModel(
    var foreignKeyName: String = "",
    var parentTable: EntityModel? = null,
    var column: FieldModel? = null,
    var parentColumn: FieldModel? = null,
    var onUpdate: UpdateStrategy = UpdateStrategy.RESTRICT,
    var onDelete: UpdateStrategy = UpdateStrategy.RESTRICT
)