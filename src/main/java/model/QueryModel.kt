package model

data class QueryModel(
    val queryType: AsyncQueryType
)