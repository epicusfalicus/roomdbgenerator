package generator.writer

import freemarker.template.Configuration
import storage.Storage.generatePath
import java.io.File
import java.io.FileWriter
import java.io.OutputStreamWriter
import java.io.Writer

class FileWriter {

    private var cfg = Configuration(Configuration.VERSION_2_3_27)

    fun createConfig(filePath: String) {
        cfg.setDirectoryForTemplateLoading(File(filePath))
    }

    fun writeInFile(fileName: String, fileTemplate: String, root: MutableMap<String, Any>) {
        val temp = cfg.getTemplate(fileTemplate)
        val out: Writer = OutputStreamWriter(System.out)
        temp.process(root, out)

        try {
            val file = FileWriter(File("$generatePath$fileName"))
            temp.process(root, file)
            file.flush()
        } catch (e: Exception) {
            println("Error")
        }

    }

}