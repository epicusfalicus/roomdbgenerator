package generator.dao

import common.firstSignToLowerCase
import generator.writer.FileWriter
import model.EntityModel
import java.util.*
import kotlin.collections.HashMap

class DaoFileGenerator {

    companion object {
        private const val GENERATED_DAO_DIRECTORY = "/Users/artemartemev/IdeaProjects/test4/src/main/java/templates"
        private const val DAO_TEMPLATE = "DaoTemplate.ftl"

        fun createDaoNameFromEntity(entityName: String): String = "${entityName}Dao"
    }

    fun createDaoFile(entity: EntityModel) {
        val root: MutableMap<String, Any> = HashMap()
        var daoName: String?
        with(entity.entityName) {
            daoName = createDaoNameFromEntity(this)
            root["daoName"] = daoName ?: ""
            root["entityLowerName"] = this.firstSignToLowerCase()
            root["entityName"] = this
            root["entityNameCaps"] = this.uppercase(Locale.getDefault())
        }
        root["package"] = "testGenerated"
        root["asyncName"] = entity.generateDefaultQuery.queryType.asyncName

        val primaryKeyField = entity.fields.findLast { it.primaryKey }
        root["primaryKey"] = primaryKeyField?.fieldName ?: ""
        root["primaryKeyType"] = primaryKeyField?.fieldType?.value ?: ""

        daoName?.let {
            val fileWriter = FileWriter()
            fileWriter.createConfig(GENERATED_DAO_DIRECTORY)
            fileWriter.writeInFile("$it.kt", DAO_TEMPLATE, root)
        }
    }
}