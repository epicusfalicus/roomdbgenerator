package generator.entity

import common.firstSignToLowerCase
import generator.writer.FileWriter
import model.EntityModel
import model.FieldAppearance
import model.ForeignKeyModel
import java.util.*
import kotlin.collections.HashMap

class EntityGenerator {

    companion object {
        private const val GENERATED_ENTITY_DIRECTORY = "/Users/artemartemev/IdeaProjects/test4/src/main/java/templates"
        private const val ENTITY_TEMPLATE = "EntityTemplate.ftl"
    }

    fun createEntityClass(model: EntityModel, targetDir: String) {
        val root: MutableMap<String, Any> = HashMap()
        val entityName = model.entityName

        root["package"] = targetDir
        root["entityName"] = entityName
        root["entityNameLower"] = entityName.firstSignToLowerCase()
        root["entityNameCaps"] = entityName.uppercase(Locale.getDefault())

        val fieldsAppearance = mutableListOf<FieldAppearance>()

        model.fields.forEach { field ->
            fieldsAppearance.add(
                FieldAppearance(
                    field,
                    field.fieldName.uppercase(Locale.getDefault()),
                    field.fieldName.firstSignToLowerCase(),
                    field.fieldType.value,
                )
            )
        }

        root["fields"] = fieldsAppearance

        model.foreignKeyModel?.let { foreignKeyModel ->
            fillForeignKeyModel(root, foreignKeyModel)
        }

        val fileWriter = FileWriter()

        fileWriter.createConfig(GENERATED_ENTITY_DIRECTORY)
        fileWriter.writeInFile("$entityName.kt", ENTITY_TEMPLATE, root)
    }
}

private fun fillForeignKeyModel(root: MutableMap<String, Any>,foreignKeyModel: ForeignKeyModel) {
    root["parentTableName"] = foreignKeyModel.parentTable?.entityName.toString()
    root["parentColumnConstant"] = foreignKeyModel.parentColumn?.fieldName?.uppercase(Locale.getDefault()) ?: ""
    root["referenceColumnConstant"] = foreignKeyModel.column?.fieldName?.uppercase(Locale.getDefault()) ?: ""
    root["deleteStrategy"] = foreignKeyModel.onDelete.value
    root["updateStrategy"] = foreignKeyModel.onUpdate.value
}