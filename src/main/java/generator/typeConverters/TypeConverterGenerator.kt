package generator.typeConverters

import generator.writer.FileWriter
import model.FieldType

class TypeConverterGenerator {

    companion object {
        private const val GENERATED_TYPE_CONVERTER_DIRECTORY =
            "/Users/artemartemev/IdeaProjects/test4/src/main/java/templates/typeConverters"
        private const val TYPE_CONVERTER_TEMPLATE = "CustomListTypeConverterTemplate.ftl"
    }

    fun execute(fieldType: FieldType) {
        val className: String
        val typeName: String

        when (fieldType) {
            FieldType.LIST_BOOLEAN -> {
                typeName = "Boolean"
                className = getConverterClassName(typeName)
            }
            FieldType.LIST_INT -> {
                typeName = "Int"
                className = getConverterClassName(typeName)
            }
            FieldType.LIST_STRING -> {
                typeName = "String"
                className = getConverterClassName(typeName)
            }
            is FieldType.LIST_CUSTOM -> {
                typeName = getTypeNameByPath(fieldType.customValue)
                className = getClassNameForCustomType(fieldType.customValue)
            }
            else -> return
        }

        val root: MutableMap<String, Any> = HashMap()
        root["className"] = className
        root["typeName"] = typeName

        val fileWriter = FileWriter()
        fileWriter.createConfig(GENERATED_TYPE_CONVERTER_DIRECTORY)
        fileWriter.writeInFile("$className.kt", TYPE_CONVERTER_TEMPLATE, root)
    }

    private fun getClassNameForCustomType(path: String): String {
        val typeName = getTypeNameByPath(path)

        return getConverterClassName(typeName)
    }

    private fun getConverterClassName(className: String) = "${className}ListConverter"

    private fun getTypeNameByPath(path: String): String {
        val paths = path.split(".")

        return paths[paths.size - 2]
    }
}