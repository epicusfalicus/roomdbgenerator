package generator.db

import common.firstSignToLowerCase
import generator.writer.FileWriter
import model.DbModel
import storage.Storage

class DbGenerator {

    companion object {
        private const val GENERATED_DB_DIRECTORY = "/Users/artemartemev/IdeaProjects/test4/src/main/java/templates/db"
        private const val DB_TEMPLATE = "DbTemplate.ftl"
        private const val DEFAULT_DB_NAME = "MyDatabase"
    }

    fun createDbFile(dbModel: DbModel) {
        val root: MutableMap<String, Any> = HashMap()
        val databaseName = dbModel.dbName ?: DEFAULT_DB_NAME

        val dao = hashMapOf<String, String>()
        dbModel.daoNameList.forEach {
            dao[it.firstSignToLowerCase()] = it
        }

        root["package"] = Storage.databaseName
        root["databaseName"] = databaseName
        root["databaseLowerName"] = databaseName.firstSignToLowerCase()
        root["entities"] = dbModel.entityNameList
        root["daoList"] = dao

        val fileWriter = FileWriter()

        fileWriter.createConfig(GENERATED_DB_DIRECTORY)
        fileWriter.writeInFile("${databaseName}.kt", DB_TEMPLATE, root)
    }

}