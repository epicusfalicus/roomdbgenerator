package generator.relationship

import common.firstSignToLowerCase
import generator.writer.FileWriter
import model.RelationshipModel
import model.RelationshipType
import java.util.*
import kotlin.collections.HashMap

class RelationshipGenerator {

    companion object {
        private const val GENERATED_RELATIONSHIP_DIRECTORY = "/Users/artemartemev/IdeaProjects/test4/src/main/java/templates"
        private const val ONE_TO_ONE_TEMPLATE = "OneToOneTemplate.ftl"
        private const val ONE_TO_MANY_TEMPLATE = "OneToManyTemplate.ftl"
        private const val MANY_TO_MANY_JUNCTION_TEMPLATE = "ManyToManyJunctionTemplate.ftl"
        private const val MANY_TO_MANY_CROSS_REF_TEMPLATE = "ManyToManyCrossRefTemplate.ftl"
    }

    fun execute(model: RelationshipModel) {
        val root: MutableMap<String, Any> = HashMap()
        val fileWriter = FileWriter()
        fileWriter.createConfig(GENERATED_RELATIONSHIP_DIRECTORY)
        // todo package
        when (model.relationshipType) {
            RelationshipType.ONE_TO_ONE -> {
                fillOneToOneRel(model, root)
                fileWriter.writeInFile("${root["relClassName"]}.kt", ONE_TO_ONE_TEMPLATE, root)
            }
            RelationshipType.ONE_TO_MANY, RelationshipType.MANY_TO_ONE -> {
                fillOneToManyRel(model, root)
                fileWriter.writeInFile("${root["relClassName"]}.kt", ONE_TO_MANY_TEMPLATE, root)
            }
            RelationshipType.MANY_TO_MANY -> {
                fillManyToManyJunction(model, root)
                fillManyToManyCrossRef(model, root)

                fileWriter.writeInFile("${model.relationshipName}Junction.kt", MANY_TO_MANY_JUNCTION_TEMPLATE, root)
                fileWriter.writeInFile("${root["crossRefTableName"]}.kt", MANY_TO_MANY_CROSS_REF_TEMPLATE, root)
            }
        }
    }

    fun fillOneToOneRel(relModel: RelationshipModel, root: MutableMap<String, Any>) {
        root["parentTableLowerName"] = relModel.parentTable?.entityName?.firstSignToLowerCase() ?: ""
        root["parentTableName"] = relModel.parentTable?.entityName ?: ""
        root["relClassName"] =
            "${relModel.parentTable?.entityName?.removeSuffix("Local")}${relModel.childTable?.entityName?.removeSuffix("Local")}"
        root["parentTableColumnConst"] = relModel.parentColumn?.fieldName?.uppercase(Locale.getDefault()) ?: ""
        root["childTableColumnConst"] = relModel.childColumn?.fieldName?.uppercase(Locale.getDefault()) ?: ""
        root["childTableLowerName"] = relModel.childTable?.entityName?.firstSignToLowerCase() ?: ""
        root["childTableName"] = relModel.childTable?.entityName ?: ""
    }

    fun fillOneToManyRel(relModel: RelationshipModel, root: MutableMap<String, Any>) {
        root["parentTableLowerName"] = relModel.parentTable?.entityName?.firstSignToLowerCase() ?: ""
        root["parentTableName"] = relModel.parentTable?.entityName ?: ""
        root["relClassName"] =
            "${relModel.parentTable?.entityName?.removeSuffix("Local")}${relModel.childTable?.entityName?.removeSuffix("Local")}"
        root["parentTableColumnConst"] = relModel.parentColumn?.fieldName?.uppercase(Locale.getDefault()) ?: ""
        root["childTableColumnConst"] = relModel.childColumn?.fieldName?.uppercase(Locale.getDefault()) ?: ""
        root["childTableLowerName"] = relModel.childTable?.entityName?.firstSignToLowerCase() ?: ""
        root["childTableName"] = relModel.childTable?.entityName ?: ""
    }

    fun fillManyToManyCrossRef(relModel: RelationshipModel, root: MutableMap<String, Any>) {
        root["parentTableColumnConst"] = relModel.parentColumn?.fieldName?.uppercase(Locale.getDefault()) ?: ""
        root["entityTableColumnConst"] = relModel.childColumn?.fieldName?.uppercase(Locale.getDefault()) ?: ""
        root["parentTableColumnLowerName"] = relModel.parentColumn?.fieldName ?: ""
        root["parentTableColumnType"] = relModel.parentColumn?.fieldType?.value ?: ""
        root["entityTableColumnLowerName"] = relModel.childColumn?.fieldName ?: ""
        root["entityTableColumnType"] = relModel.childColumn?.fieldType?.value ?: ""
        root["crossRefTableName"] =
            "${relModel.parentTable?.entityName?.removeSuffix("Local")}" +
                "${relModel.childTable?.entityName?.removeSuffix("Local")}CrossRef"
    }

    fun fillManyToManyJunction(relModel: RelationshipModel, root: MutableMap<String, Any>) {
        root["parentTableLowerName"] = relModel.parentTable?.entityName?.firstSignToLowerCase() ?: ""
        root["parentTableName"] = relModel.parentTable?.entityName ?: ""
        root["parentTableColumnConst"] = relModel.parentColumn?.fieldName?.uppercase(Locale.getDefault()) ?: ""
        root["entityTableName"] = relModel.childTable?.entityName ?: ""
        root["entityTableLowerName"] = relModel.childTable?.entityName?.firstSignToLowerCase() ?: ""
        root["parentTableColumnConst"] = relModel.childColumn?.fieldName?.uppercase(Locale.getDefault()) ?: ""
        root["crossRefTableName"] =
            "${relModel.parentTable?.entityName?.removeSuffix("Local")}" +
                "${relModel.childTable?.entityName?.removeSuffix("Local")}CrossRef"
    }
}