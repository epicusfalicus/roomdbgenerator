package testData

//@Entity(
// tableName = INSPECTION_INFO_TABLE
// primaryKeys = [INSPECTION_ID]
// )
data class InspectionInfoLocal(
    //    @ColumnInfo(name = INSPECTION_ID)
    val inspectionId: String,
    //    @ColumnInfo(name = INSPECTION_NAME)
    val inspectionName: String,
    //    @ColumnInfo(name = PLACE)
    val place: String,
    //    @ColumnInfo(name = START_DATE)
    val startDate: String,
    //    @ColumnInfo(name = END_DATE)
    val endDate: String,
    //    @ColumnInfo(name = STATUS)
    val status: String,
    //    @ColumnInfo(name = DURATION)
    val duration: String
) {
    companion object {
        const val INSPECTION_INFO_TABLE = "inspection_info_table"
        const val INSPECTION_NAME = "inspection_name"
        const val INSPECTION_ID = "inspection_id"
        const val PLACE = "place"
        const val START_DATE = "start_date"
        const val END_DATE = "end_date"
        const val STATUS = "status"
        const val DURATION = "duration"
    }
}